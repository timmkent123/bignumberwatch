//
//  InterfaceController.swift
//  BigNumberWatch WatchKit Extension
//
//  Created by Marc Felden on 03/11/2019.
//  Copyright © 2019 Marc Felden. All rights reserved.
//

import WatchKit
import Foundation


class InterfaceController: WKInterfaceController {

    
    @IBOutlet weak var nextBoyInstallsToday: WKInterfaceLabel!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        
        var request = URLRequest(url: URL(string:"https://dashboard-de8e7.firebaseio.com/nextboy/statistics/2019-11-03/deviceids/count.json")!)
        request.httpMethod = "GET"
        
        let task1 = URLSession.shared.dataTask(with: request) {
            data, response, error in
            if let data = data {
            if let result = String(data: data, encoding: String.Encoding.utf8) {
                self.nextBoyInstallsToday.setText(result)
                }
            }
         
        }
        task1.resume()

    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func dateStringTodayPST() -> String {
        let today = Date()
        
        let df = DateFormatter()
        df.timeZone = TimeZone(abbreviation: "PST")
        df.dateFormat = "yyyy-MM-dd"
        let date = df.string(from: today)
        return date
    }

}
